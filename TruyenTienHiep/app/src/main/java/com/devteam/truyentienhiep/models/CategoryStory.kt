package com.devteam.truyentienhiep.models

import android.arch.persistence.room.Entity

/**
 * TruyenTienHiep - com.devteam.truyentienhiep.models
 * Created by Kerofrog on 7/9/17.
 */

@Entity(primaryKeys = arrayOf("categoryId", "storyId"), tableName = "CATEGORY_STORY")
data class CategoryStory(
        var categoryId: Long = 0,
        var storyId: Long = 0
)
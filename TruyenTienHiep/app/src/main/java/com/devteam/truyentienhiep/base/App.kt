package com.devteam.truyentienhiep.base

import android.app.Application
import android.arch.persistence.room.Room
import com.facebook.stetho.Stetho


/**
 * TruyenTienHiep - com.devteam.truyentienhiep.base
 * Created by Kerofrog on 7/9/17.
 */

class App : Application() {


    override fun onCreate() {
        super.onCreate()
        folderPath = if (filesDir.absolutePath.endsWith("/")) filesDir.absolutePath else filesDir.absolutePath + "/"
        database = Room.databaseBuilder(this, RoomDb::class.java, "truyen-db").build()
        Stetho.initializeWithDefaults(this)

    }

    companion object {
        lateinit var database: RoomDb
        lateinit var folderPath: String
    }
}

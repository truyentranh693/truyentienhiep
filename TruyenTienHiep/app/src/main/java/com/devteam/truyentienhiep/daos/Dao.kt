package com.devteam.truyentienhiep.daos

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.devteam.truyentienhiep.models.Category
import com.devteam.truyentienhiep.models.CategoryStory
import com.devteam.truyentienhiep.models.Story

/**
 * TruyenTienHiep - com.devteam.truyentienhiep.daos
 * Created by Kerofrog on 7/9/17.
 */
/**
 *
 *
 * @Dao
interface PersonDao {

@Query("SELECT * FROM person")
fun getAllPeople(): Flowable<List<Person>>

@Insert
fun insert(person: Person)
}

@D*/

@Dao
interface CategoryDao {
    @Query("SELECT * FROM CATEGORY")
    fun loadAll(): List<Category>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(category: Category): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(cates: List<Category>)
}

@Dao
interface StoryDao {
    @Query("SELECT * FROM STORY")
    fun loadAll(): List<Story>

    @Query("SELECT * FROM STORY WHERE id IN (SELECT storyId FROM CATEGORY_STORY WHERE categoryId = (:arg0))")
    fun loadFromCategory(categoryId: Long): List<Story>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(story: Story)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(stories: List<Story>)


}

@Dao
interface CategoryStoryDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(categoryStory: CategoryStory)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(categoryStories: List<CategoryStory>)

}

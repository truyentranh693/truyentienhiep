package com.devteam.truyentienhiep.views;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;

/**
 * TruyenTienHiep - com.devteam.truyentienhiep.views
 * Created by Kerofrog on 7/9/17.
 */

public class VerticalImageView extends AppCompatImageView {


    public VerticalImageView(Context context) {
        super(context);
    }

    public VerticalImageView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public VerticalImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = MeasureSpec.getSize(widthMeasureSpec);


        this.setMeasuredDimension(width, width * 3 / 2);
    }
}

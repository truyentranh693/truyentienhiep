package com.devteam.truyentienhiep.services

import com.devteam.truyentienhiep.base.App
import com.google.firebase.storage.FirebaseStorage
import java.io.File


/**
 * TruyenTienHiep - com.devteam.truyentienhiep.services
 * Created by Kerofrog on 7/9/17.
 */
object FirebaseService {

    val CATEGORIES_FILE_NAME = "list.json"

    fun readFile(filePath: String, completion: (result: String) -> Unit, error: (error: Throwable) -> Unit) {

        downloadFile(filePath, App.folderPath + "temp.json", {
            try {
                val content = FileHandler.readFile(it)
                completion(content)
            } catch (e: Exception) {
                error(e)
            }
        }, error)
    }

    fun downloadFile(filePath: String, localPath: String, completion: ((file: File) -> Unit)?, error: ((error: Throwable) -> Unit)?) {
        val file = File(localPath)
        FirebaseStorage.getInstance()
                .reference.child(filePath)
                .getFile(file)
                .addOnCompleteListener {
                    try {
                        completion?.invoke(file)
                    } catch (e: Exception) {
                        error?.invoke(e)
                    }
                }
                .addOnFailureListener {
                    error?.invoke(it)
                }
    }

}
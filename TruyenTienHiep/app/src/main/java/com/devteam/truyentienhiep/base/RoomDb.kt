package com.devteam.truyentienhiep.base

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.devteam.truyentienhiep.daos.CategoryDao
import com.devteam.truyentienhiep.daos.CategoryStoryDao
import com.devteam.truyentienhiep.daos.StoryDao
import com.devteam.truyentienhiep.models.Category
import com.devteam.truyentienhiep.models.CategoryStory
import com.devteam.truyentienhiep.models.Story

/**
 * TruyenTienHiep - com.devteam.truyentienhiep.base
 * Created by Kerofrog on 7/9/17.
 */
@Database(entities = arrayOf(Category::class, Story::class, CategoryStory::class), version = 1)
abstract class RoomDb : RoomDatabase() {
    abstract fun categoryDao(): CategoryDao
    abstract fun storyDao(): StoryDao
    abstract fun categoryStoryDao(): CategoryStoryDao
}
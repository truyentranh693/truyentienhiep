package com.devteam.truyentienhiep.use_case.stories

import android.os.Handler
import android.os.Looper
import com.devteam.truyentienhiep.base.App
import com.devteam.truyentienhiep.models.Category
import com.devteam.truyentienhiep.models.CategoryStory
import com.devteam.truyentienhiep.models.Story
import com.devteam.truyentienhiep.services.FirebaseService
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlin.concurrent.thread

/**
 * TruyenTienHiep - com.devteam.truyentienhiep.use_case.stories
 * Created by Kerofrog on 7/9/17.
 */

interface ListStoryView {
    fun showLoadingCategories()
    fun hideLoadingCategories()

    fun showLoadingStories()
    fun hideLoadingStories()

    fun setCategories(categories: List<Category>)
    fun setStories(stories: List<Story>)

}


class ListStoryPresenter(var view: ListStoryView?, val model: ListStoryModel) {

    fun loadCategories() {
        view?.showLoadingCategories()

        model.loadCategories({
            view?.setCategories(it)
            view?.hideLoadingCategories()
        }, {
            log("Error $it")
            view?.hideLoadingCategories()
        })
    }

    fun onCategorySelected(category: Category?) {
        thread {
            val s = App.database.storyDao().loadAll()
            Handler(Looper.getMainLooper()).post { view?.setStories(s) }
        }
    }

    fun onDetach() {
        view = null
    }


}


class ListStoryModel {

    fun loadCategories(completion: (result: List<Category>) -> Unit, error: (error: Throwable) -> Unit) {

        val handler = Handler(Looper.getMainLooper())
        thread {

            val list = App.database.categoryDao().loadAll()
            if (list.isEmpty()) {
                log("IF")
                FirebaseService.readFile(FirebaseService.CATEGORIES_FILE_NAME, {
                    try {
                        val remoteList = Gson().fromJson<List<Category>>(it, object : TypeToken<List<Category>>() {}.type)
                        thread {
                            App.database.categoryDao().insertAll(remoteList)
                        }
                        loadStoryFromCategories(remoteList.toMutableList())
                        handler.post { completion(remoteList) }
                    } catch (e: Exception) {
                        error(e)
                    }
                }, error)
            } else {
                log("ELSE")
                completion(list)
                loadStoryFromCategories(list.toMutableList())
            }
        }
    }

    fun loadStoryFromCategories(categories: MutableList<Category>) {
        if (categories.isNotEmpty()) {
            val category = categories[0]
            FirebaseService.readFile(category.json, {
                try {
                    val stories = Gson().fromJson<List<Story>>(it, object : TypeToken<List<Story>>() {}.type)
                    thread {
                        App.database.storyDao().insertAll(stories)
                        App.database.categoryStoryDao().insertAll(stories.map {
                            CategoryStory(category.id, it.id)
                        }.toList())
                    }
                    log("DONE INSERT ${category.type}")
                    categories.remove(category)
                    loadStoryFromCategories(categories)
                } catch (e: Exception) {
                    log("Error2 loadStoryFromCategories: $e")
                }
            }, {
                log("Error loadStoryFromCategories: $it")
            })
        }


    }
}

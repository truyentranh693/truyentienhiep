package com.devteam.truyentienhiep.use_case.stories

import android.app.ProgressDialog
import android.databinding.DataBindingUtil
import android.graphics.Rect
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.BaseAdapter
import com.devteam.pm.projectmanagement.adapter.RecyclerAdapter
import com.devteam.truyentienhiep.BR
import com.devteam.truyentienhiep.R
import com.devteam.truyentienhiep.databinding.ActivityMainBinding
import com.devteam.truyentienhiep.models.Category
import com.devteam.truyentienhiep.models.Story
import com.devteam.truyentienhiep.utils.ResUtil


class ListStoriesActivity : AppCompatActivity(), ListStoryView {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        presenter.loadCategories()


        binding.recyclerView.layoutManager = LinearLayoutManager(this)
        binding.recyclerView.addItemDecoration(ListDecorator())

        binding.spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
            }

        }

    }


    var categoryDialog: ProgressDialog? = null
    override fun showLoadingCategories() {
        categoryDialog = ProgressDialog.show(this, "showLoadingStories", "showLoadingStories", true, true)
    }

    override fun hideLoadingCategories() {
        categoryDialog?.dismiss()
    }


    override fun showLoadingStories() {

    }

    override fun hideLoadingStories() {
    }

    override fun setCategories(categories: List<Category>) {
        binding.spinner.adapter = ArrayAdapter<Category>(this, android.R.layout.simple_spinner_dropdown_item, categories)
    }

    override fun setStories(stories: List<Story>) {
        log("SET STORIES: ${stories.size}")
        binding.recyclerView.adapter = RecyclerAdapter(this, stories.toMutableList(), BR.story, R.layout.row_story)
    }

    lateinit var binding: ActivityMainBinding
    val presenter by lazy {
        ListStoryPresenter(this, ListStoryModel())
    }

    override fun onDestroy() {
        presenter.onDetach()
        super.onDestroy()
    }


    class ListDecorator : RecyclerView.ItemDecoration() {

        override fun getItemOffsets(outRect: Rect?, view: View?, parent: RecyclerView?, state: RecyclerView.State?) {
            val spacing = ResUtil.dpToPx(8)
            val position = parent?.getChildLayoutPosition(view)!!
            if (position == 0) {
                outRect?.top = spacing
            } else {
                outRect?.top = spacing / 2
            }

            outRect?.left = spacing
            outRect?.right = spacing
            outRect?.bottom = spacing / 2
        }


    }

//    class CategoryAdapter : BaseAdapter() {
//        override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {
//        }
//
//        override fun getItem(p0: Int): Any {
//        }
//
//        override fun getItemId(p0: Int): Long {
//        }
//
//        override fun getCount(): Int {
//        }
//
//    }

}

fun log(message: String) {
    Log.e("TAG", message)
}

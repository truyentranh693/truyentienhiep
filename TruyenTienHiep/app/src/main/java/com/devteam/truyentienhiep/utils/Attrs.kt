package com.devteam.truyentienhiep.utils

import android.databinding.BindingAdapter
import android.os.Build
import android.text.Html
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.devteam.truyentienhiep.use_case.stories.log
import com.google.firebase.storage.FirebaseStorage


/**
 * TruyenTienHiep - com.devteam.truyentienhiep.utils
 * Created by Kerofrog on 7/9/17.
 */


@BindingAdapter("url")
fun setImage(img: ImageView, path: String) {

    val ref = FirebaseStorage.getInstance().reference.child(path)
    ref.downloadUrl.addOnSuccessListener {
        Glide.with(img.context)
                .load(it.toString())
                .into(img)
    }.addOnFailureListener {
        log("ex addOnFailureListener $it")
    }
}

@BindingAdapter("html")
fun setText(tv: TextView, text: String) {
    if (Build.VERSION.SDK_INT >= 24) {
        tv.text = Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY)
    } else {
        tv.text = Html.fromHtml(text)
    }
}
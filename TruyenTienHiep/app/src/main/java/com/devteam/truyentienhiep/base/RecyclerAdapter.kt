package com.devteam.pm.projectmanagement.adapter

import android.content.Context
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.support.annotation.LayoutRes
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.devteam.truyentienhiep.use_case.stories.log

/**
 * SportsPass - com.sportspass.app.adapters
 * Created by Kerofrog on 2/6/17.
 */

class RecyclerAdapter<T>(context: Context, var items: MutableList<T>, val variableID: Int, @LayoutRes val layout: Int) : RecyclerView.Adapter<RecyclerAdapter.BindingHolder<T>>() {
    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var onItemClickListener: OnItemClickListener<T>? = null

    fun setOnItemClickListener(onItemClickListener: OnItemClickListener<T>) {
        this.onItemClickListener = onItemClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BindingHolder<T> {
        return BindingHolder(inflater.inflate(layout, parent, false))
    }

    override fun onBindViewHolder(holder: BindingHolder<T>, position: Int) {
        val item = items[position]
        holder.binding.setVariable(variableID, item)
        holder.binding.executePendingBindings()
        holder.setOnItemClickListener(item, onItemClickListener)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun updateList(items: MutableList<T>, result: DiffUtil.DiffResult) {
        log("update list")
        this.items = items
        result.dispatchUpdatesTo(this)
    }

    fun add(resource: T) {
        items.add(resource)
        notifyItemInserted(items.size - 1)
    }

    class BindingHolder<T>(rowView: View) : RecyclerView.ViewHolder(rowView) {
        val binding: ViewDataBinding = DataBindingUtil.bind<ViewDataBinding>(rowView)

        fun setOnItemClickListener(item: T, onItemClickListener: OnItemClickListener<T>?) {
            itemView.setOnClickListener { onItemClickListener?.onClick(item) }
        }
    }

    interface OnItemClickListener<in T> {
        fun onClick(item: T)
    }
}


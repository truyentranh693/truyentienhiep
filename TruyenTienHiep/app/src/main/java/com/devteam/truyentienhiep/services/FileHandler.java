package com.devteam.truyentienhiep.services;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

/**
 * TruyenTienHiep - com.devteam.truyentienhiep.services
 * Created by Kerofrog on 7/9/17.
 */

public class FileHandler {

    public static String readFile(File file) throws Exception {
        //Read text from file
        StringBuilder text = new StringBuilder();
        BufferedReader br = new BufferedReader(new FileReader(file));
        String line;

        while ((line = br.readLine()) != null) {
            text.append(line);
            text.append('\n');
        }
        br.close();
        return text.toString();
    }

}

package com.devteam.truyentienhiep.models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * TruyenTienHiep - com.devteam.truyentienhiep.models
 * Created by Kerofrog on 7/9/17.
 */
@Entity
data class Category(
        @PrimaryKey(autoGenerate = false)
        var id: Long = 0,
        var type: String = "",
        var json: String = "") {

    override fun toString(): String {
        return type
    }

}

package com.devteam.truyentienhiep.models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * TruyenTienHiep - com.devteam.truyentienhiep.models
 * Created by Kerofrog on 7/9/17.
 */

@Entity
data class Story(
        @PrimaryKey(autoGenerate = false)
        var id: Long = 0,
        var name: String = "",
        var description: String = "",
        var zip: String = "",
        var image: String = "",
        var author: String = "",
        var type: String = "",
        var chapterCount: Int = 0,
        var versionCode: Int = 1) {

}

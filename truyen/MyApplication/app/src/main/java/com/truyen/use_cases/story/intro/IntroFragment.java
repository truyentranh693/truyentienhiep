package com.truyen.use_cases.story.intro;


import android.app.ProgressDialog;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.truyen.R;
import com.truyen.base.App;
import com.truyen.base.AppFragment;
import com.truyen.databinding.FragmentIntroBinding;
import com.truyen.models.Story;
import com.truyen.use_cases.chaps.ChapActivity;
import com.truyen.use_cases.story.DownloadListener;
import com.truyen.util.Log;

import static android.view.View.VISIBLE;

/**
 * MyApplication - com.truyen.use_cases.story
 * Created by Kerofrog on 7/11/17.
 */

public class IntroFragment extends AppFragment implements IntroView {

    IntroPresenter presenter;

    public static IntroFragment newInstance(Story story) {
        return new IntroFragment();
    }


    FragmentIntroBinding binding;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_intro, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Story story = App.getStory(getContext());
        binding.setStory(story);
        presenter = new IntroPresenter(this, story);

        binding.likeButton.setOnClickListener(v -> presenter.onLikeClick());
        binding.readButton.setOnClickListener(v -> presenter.onReadClick());
        binding.downloadButton.setOnClickListener(v -> presenter.onDownloadClick());
        presenter.attach();
        Log.e(story.toString());
    }

    @Override
    public void onDestroy() {
        if (presenter != null) {
            presenter.detach();
        }
        super.onDestroy();
    }

    @Override
    public void showContinueReadingAlert() {
        new AlertDialog.Builder(getContext())
                .setTitle(R.string.continue_reading)
                .setMessage(R.string.continue_reading_description)
                .setNegativeButton(R.string.no_thanks, (dialogInterface, i) -> presenter.onContinueReadingCancel())
                .setPositiveButton(R.string.continue_reading, (dialogInterface, i) -> presenter.onContinueReadingOk()).create().show();
    }

    ProgressDialog downloadProgressDialog;

    @Override
    public void showDownloadProgress() {
        downloadProgressDialog = new ProgressDialog(getContext());
        downloadProgressDialog.setCancelable(false);
        downloadProgressDialog.setTitle(getString(R.string.downloading));
        downloadProgressDialog.setIndeterminate(false);
        downloadProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        downloadProgressDialog.show();
    }

    @Override
    public void updateDownloadProgress(int progress) {
        downloadProgressDialog.setProgress(progress);
    }

    @Override
    public void dismissDownloadProgress() {
        if (downloadProgressDialog != null && downloadProgressDialog.isShowing()) {
            downloadProgressDialog.dismiss();
            downloadProgressDialog = null;
        }
    }

    @Override
    public void showErrorDownload(String error) {
        new AlertDialog.Builder(getContext())
                .setTitle(R.string.error_occur)
                .setMessage(R.string.no_internet)
                .setPositiveButton(R.string.ok, null)
                .create().show();
    }

    @Override
    public void readChap(Story story, int chapter) {
        ChapActivity.start(getContext(), story, chapter);
    }

    @Override
    public void refreshActivity() {
        presenter.attach();
        if (getActivity() instanceof DownloadListener) {
            ((DownloadListener) getActivity()).onDownloaded();
        }
    }

    @Override
    public void showLikeNormal() {
        binding.likeImage.setImageResource(R.drawable.ic_like);
    }

    @Override
    public void showLikeEnabled() {
        binding.likeImage.setImageResource(R.drawable.ic_like_enable);
    }

    @Override
    public void showReadingDisable() {
        binding.readButton.setVisibility(View.GONE);
    }

    @Override
    public void showReadingEnable() {
        binding.readButton.setVisibility(VISIBLE);
    }

    @Override
    public void showDownloadDisable() {
        binding.downloadButton.setVisibility(View.GONE);
    }

    @Override
    public void showDownloadEnable() {
        binding.downloadButton.setVisibility(VISIBLE);
    }
}

package com.truyen.util;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.orhanobut.logger.Logger;

import java.util.List;
import java.util.Map;

public class MapUtil {

    private Map<String, Object> map;

    private MapUtil(Map<String, Object> map) {
        this.map = map;
    }

    public static MapUtil of(Map<String, Object> map) {
        return new MapUtil(map);
    }

    public void set(Map<String, Object> map) {
        this.map = map;
    }

    public void set(Object map) {
        try {
            this.map = (Map<String, Object>) map;
        } catch (Exception e) {

        }
    }

    public Map<String, Object> get() {
        return map;
    }

    @NonNull
    public String getString(String key) {
        if (map == null) {
            return "";
        }
        Object obj = map.get(key);
        if (obj instanceof String) {
            return (String) obj;
        }
        return "";
    }

    public long getLong(String key) {
        if (map == null) {
            return 0;
        }
        Object obj = map.get(key);
        if (obj instanceof Long) {
            return (Long) obj;
        }
        return 0;
    }

    public int getInt(String key) {
        if (map == null) {
            return 0;
        }
        Object obj = map.get(key);
        if (obj instanceof Long) {
            return ((Long) obj).intValue();
        }
        return 0;
    }


    public double getDouble(String key) {
        if (map == null) {
            Logger.e("Map null cmnr");
            return 0;
        }
        Object obj = map.get(key);
        if (obj instanceof Double) {
            Logger.e("Map null cmnr 2");
            return (Double) obj;
        }
        Logger.e("Map null cmnr 3: " + obj);
        return 0;
    }

    @Nullable
    public List<Map<String, Object>> getList(String key) {
        if (map == null) {
            return null;
        }
        try {
            return (List<Map<String, Object>>) map.get(key);
        } catch (Exception e) {
            return null;
        }
    }
}

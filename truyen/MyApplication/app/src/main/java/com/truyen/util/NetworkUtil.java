package com.truyen.util;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class NetworkUtil {

    public static Flowable<Boolean> isInternetAvailable() {
        return Flowable.fromCallable(() -> {
            try {
                HttpURLConnection connection = (HttpURLConnection) (new URL("http://connectivitycheck.gstatic.com/generate_204")
                        .openConnection());
                connection.setConnectTimeout(2000);
                connection.setReadTimeout(2000);
                connection.connect();
                return (connection.getResponseCode() == HttpURLConnection.HTTP_NO_CONTENT);
            } catch (IOException e) {
                e.printStackTrace();
                return (false);
            }
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

    }

}

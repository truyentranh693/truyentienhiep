package com.truyen.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.annimon.stream.Optional;
import com.google.firebase.database.DataSnapshot;
import com.orhanobut.logger.Logger;
import com.truyen.util.MapUtil;

import java.util.Map;

@Entity
public class Category {
    @PrimaryKey
    private long id = 0;
    private String type = "";
    private long updatedAt;
    private long storyUpdatedAt;
    private String json = "";

    private Category(Builder builder) {
        setId(builder.id);
        setType(builder.type);
        setUpdatedAt(builder.updatedAt);
        setStoryUpdatedAt(builder.storyUpdatedAt);
    }

    public Category(long id, String type, long updatedAt, long storyUpdatedAt, String json) {
        this.id = id;
        this.type = type;
        this.updatedAt = updatedAt;
        this.storyUpdatedAt = storyUpdatedAt;
        this.json = json;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }

    public long getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(long updatedAt) {
        this.updatedAt = updatedAt;
    }

    public long getStoryUpdatedAt() {
        return storyUpdatedAt;
    }

    public void setStoryUpdatedAt(long storyUpdatedAt) {
        this.storyUpdatedAt = storyUpdatedAt;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public static Optional<Category> fromDataSnapshot(DataSnapshot dataSnapshot) {
        try {
            Map<String, Object> map = (Map<String, Object>) dataSnapshot.getValue();
            MapUtil m = MapUtil.of(map);
            return Optional.of(new Builder()
                    .id(m.getLong("id"))
                    .type(m.getString("type"))
                    .updatedAt(m.getLong("updatedAt"))
                    .build());
        } catch (Exception e) {
            Logger.e("exception parse category: " + e.getMessage());
        }
        return Optional.empty();
    }


    private static final class Builder {
        private long id;
        private String type;
        private long updatedAt;
        private long storyUpdatedAt;

        Builder() {
        }

        public Builder id(long val) {
            id = val;
            return this;
        }

        public Builder type(String val) {
            type = val;
            return this;
        }

        public Builder updatedAt(long val) {
            updatedAt = val;
            return this;
        }

        public Builder storyUpdatedAt(long val) {
            storyUpdatedAt = val;
            return this;
        }

        public Category build() {
            return new Category(this);
        }
    }
}

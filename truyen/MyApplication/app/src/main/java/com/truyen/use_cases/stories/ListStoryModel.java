package com.truyen.use_cases.stories;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.annimon.stream.Stream;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.orhanobut.logger.Logger;
import com.truyen.base.App;
import com.truyen.daos.StoryDao;
import com.truyen.interfaces.Completion;
import com.truyen.interfaces.OnError;
import com.truyen.interfaces.OnProgress;
import com.truyen.models.Category;
import com.truyen.models.CategoryStory;
import com.truyen.models.Story;
import com.truyen.util.FileHandler;
import com.truyen.util.FirebaseService;
import com.truyen.util.Log;
import com.truyen.util.NetworkUtil;
import com.truyen.util.ZipHelper;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

class ListStoryModel {

    private Context context;

    ListStoryModel(Context context) {
        this.context = context;
    }

    private long getCategoryUpdatedAt() {
        return context.getSharedPreferences("prefs", Context.MODE_PRIVATE).getLong("updatedAt", 0);
    }

    private void putCategoryUpdatedAt(long updatedAt) {
        context.getSharedPreferences("prefs", Context.MODE_PRIVATE)
                .edit()
                .putLong("updatedAt", updatedAt)
                .apply();
    }

    private void loadCategoryFromFb(@Nullable final Completion<List<Category>> completion, @Nullable final OnError onError) {
        FirebaseDatabase.getInstance()
                .getReference("categories")
                .orderByChild("updatedAt")
                .startAt(getCategoryUpdatedAt())
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        final List<Category> categories = new ArrayList<>();
                        for (DataSnapshot s : dataSnapshot.getChildren()) {
                            Category.fromDataSnapshot(s).executeIfPresent(categories::add);
                        }
                        new Thread(() -> App.database.getCategoryDao().insertAll(categories)).start();
                        if (completion != null) {
                            completion.onSuccess(categories);
                        }
                        if (!categories.isEmpty()) {
                            putCategoryUpdatedAt(categories.get(categories.size() - 1).getUpdatedAt());
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        if (onError != null) {
                            onError.onError(new Throwable(databaseError.getMessage()));
                        }
                    }
                });
    }

    private void checkInternetAndLoadCategories(final Completion<List<Category>> completion, final OnError onError) {
        NetworkUtil.isInternetAvailable()
                .subscribe(aBoolean -> {
                    if (aBoolean) {
                        loadCategoryFromFb(completion, onError);
                    } else {
                        onError.onError(new Throwable("Network not available"));
                    }
                });

    }

    void loadCategories(final Completion<List<Category>> completion, final OnError onError) {

        getCategories().subscribe(categories -> {
            if (categories.isEmpty()) {
                checkInternetAndLoadCategories(completion, onError);
            } else {
                completion.onSuccess(categories);
                loadCategoryFromFb(null, null);
            }
        });
    }

    private void checkInternetAndLoadStories(final Category category, final Completion<List<Story>> completion, final OnError onError) {

        NetworkUtil.isInternetAvailable()
                .subscribe(aBoolean -> {
                    if (aBoolean) {
                        loadStoriesFromFb(category, completion, onError);
                    } else {
                        onError.onError(new Throwable("Internet is not available"));
                    }
                });
    }

    private void loadStoriesFromFb(final Category category, @Nullable final Completion<List<Story>> completion, @Nullable final OnError onError) {
        FirebaseDatabase.getInstance()
                .getReference("stories")
                .child("" + category.getId())
                .orderByChild("updatedAt")
                .startAt(category.getStoryUpdatedAt())
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        List<Story> stories = new ArrayList<>();
                        for (DataSnapshot s : dataSnapshot.getChildren()) {
                            Story.fromDatasnapshot(s).executeIfPresent(stories::add);
                        }
                        insertStoriesToDb(stories, category);
                        if (completion != null) {
                            completion.onSuccess(stories);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        if (onError != null) {
                            onError.onError(new Throwable(databaseError.getMessage()));
                        }
                    }
                });
    }

    void loadStoryFromCategory(final Category category, final Completion<List<Story>> completion, final OnError onError) {

        getStories(category).subscribe(stories -> {
            if (stories.isEmpty()) {
                checkInternetAndLoadStories(category, completion, onError);
            } else {
                completion.onSuccess(stories);
            }
        });
    }

    private void insertStoriesToDb(final List<Story> stories, final Category category) {
        if (stories == null || stories.isEmpty()) {
            return;
        }
        Flowable.fromCallable(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                StoryDao dao = App.database.getStoryDao();
                dao.insertAll(stories);

                List<CategoryStory> categoryStories = Stream.of(stories)
                        .map(story -> new CategoryStory(category.getId(), story.getId())).toList();
                App.database.getCategoryStoryDao().insertAll(categoryStories);
                category.setUpdatedAt(stories.get(stories.size() - 1).getUpdatedAt());
                App.database.getCategoryDao().update(category);
                return null;
            }
        }).subscribeOn(Schedulers.io())
                .subscribe(new Consumer<Void>() {
                    @Override
                    public void accept(@io.reactivex.annotations.NonNull Void aVoid) throws Exception {
                        Logger.e("DONE");
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@io.reactivex.annotations.NonNull Throwable throwable) throws Exception {
                        Logger.e("DKM: " + throwable.getMessage());
                        throwable.printStackTrace();
                    }
                });
    }

    void download(Story story, Completion<File> completion, OnProgress progress, OnError onError) {
        final File file = new File(App.folderPath + story.getZip());
        boolean success = FileHandler.createFolderForFilePath(file.getAbsolutePath());
        if (!success) {
            onError.onError(new Exception("Cannot create file " + file.getAbsolutePath()));
            return;
        }

        Handler handler = new Handler(Looper.getMainLooper());
        FirebaseStorage.getInstance()
                .getReference().child(story.getZip())
                .getFile(file)
                .addOnCompleteListener(task -> handler.post(() -> {
                    if (!task.isSuccessful()) {
                        return;
                    }
                    FirebaseService.incDownloadCount(story);
                    try {
                        boolean x = ZipHelper.unzip2(file.getAbsolutePath(), App.folderPath);
                        Logger.e("UNZIPPPPP: " + x);
                        story.setState(Story.State.DOWNLOADED);
                        completion.onSuccess(file);
                        new Thread(() -> App.database.getStoryDao().update(story)).start();
                    } catch (Exception e) {
                        onError.onError(e);
                    }
                }))
                .addOnProgressListener(taskSnapshot -> {
                    handler.post(() -> {
                        int prog = (int) (taskSnapshot.getBytesTransferred() * 1.0f / taskSnapshot.getTotalByteCount() * 100);
                        story.setDownloadProgress(prog);
                        AsyncTask.execute(() -> App.database.getStoryDao().update(story));
                        progress.onProgress(taskSnapshot.getBytesTransferred(), taskSnapshot.getTotalByteCount(), prog);
                    });

                })
                .addOnFailureListener(e -> handler.post(() -> {
                    story.setDownloadProgress(0);
                    new Thread(() -> App.database.getStoryDao().update(story)).start();
                    onError.onError(e);
                }));

    }

    private Flowable<List<Category>> getCategories() {
        return Flowable.fromCallable(() -> App.database.getCategoryDao().loadAll())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private Flowable<List<Story>> getStories(Category category) {
        return Flowable.fromCallable(() -> App.database.getStoryDao().loadStoryOfCategory(category.getId()))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

}

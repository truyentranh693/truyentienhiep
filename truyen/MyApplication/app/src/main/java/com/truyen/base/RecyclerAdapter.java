package com.truyen.base;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.LayoutRes;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

/**
 * SportsPass - com.sportspass.app.adapters
 * Created by Kerofrog on 2/6/17.
 */

public class RecyclerAdapter<T> extends RecyclerView.Adapter<RecyclerAdapter.BindingHolder> {
    protected List<T> items;
    private LayoutInflater inflater;
    private int variableID;
    private int layout;
    private OnItemClickListener<T> onItemClickListener;
    protected Context context;

    public RecyclerAdapter(Context context, List<T> items, int variableID, @LayoutRes int layout) {
        this.items = items;
        this.variableID = variableID;
        inflater = LayoutInflater.from(context);
        this.layout = layout;
        this.context = context;
    }

    public void setOnItemClickListener(OnItemClickListener<T> onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void setItems(List<T> items, android.support.v7.util.DiffUtil.DiffResult result) {
        this.items = items;
        result.dispatchUpdatesTo(this);
    }

    public List<T> getItems() {
        return items;
    }

    public T getItem(int position) {
        return items.get(position);
    }

    @Override
    public BindingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //        Logger.z("onCreateViewHolder");
        return new BindingHolder(inflater.inflate(layout, parent, false));
    }

    @Override
    public void onBindViewHolder(BindingHolder holder, int position) {
        //        Logger.z("onBindViewHolder: " + position);

        Object user = items.get(position);
        holder.getBinding().setVariable(variableID, user);
        holder.getBinding().executePendingBindings();
        holder.setOnItemClickListener(user, onItemClickListener);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    public static class BindingHolder<T> extends RecyclerView.ViewHolder {
        private ViewDataBinding binding;

        BindingHolder(View rowView) {
            super(rowView);
            binding = DataBindingUtil.bind(rowView);
        }

        public ViewDataBinding getBinding() {
            return binding;
        }

        public void setOnItemClickListener(final T item, final OnItemClickListener<T> onItemClickListener) {
            if (onItemClickListener == null) {
                return;
            }
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemClickListener.onClick(item);
                }
            });
        }
    }

    public interface OnItemClickListener<T> {
        void onClick(T item);
    }

}

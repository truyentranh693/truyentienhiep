package com.truyen.use_cases.chaps;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.TextView;

import com.truyen.R;
import com.truyen.base.AppFragment;
import com.truyen.util.FileHandler;

import java.io.File;

/**
 * MyApplication - com.truyen.use_cases.chaps
 * Created by Kerofrog on 7/12/17.
 */

public class ChapFragment extends AppFragment {

    public static ChapFragment newInstance(String path, int chap) {

        Bundle args = new Bundle();
        args.putString("path", path);
        args.putInt("chap", chap);
        ChapFragment fragment = new ChapFragment();
        fragment.setArguments(args);
        return fragment;
    }

    TextView chapterContent;
    ScrollView scrollView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_chap, container, false);
        chapterContent = v.findViewById(R.id.chapterContent);
        scrollView = v.findViewById(R.id.scrollView);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        int chap = getArguments().getInt("chap");
        String path = getArguments().getString("path", "");
        File f = new File(path);
        String title = f.getName().replace(".txt", "");
        try {
            chapterContent.setText(Html.fromHtml("<big><big>" + title + "</big></big><br/><br/>" + FileHandler.readFile(f)));

            scrollView.getViewTreeObserver().addOnScrollChangedListener(() -> {
                if (getActivity() instanceof OnReadChap) {
                    ((OnReadChap) getActivity()).read(chap);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

package com.truyen.use_cases.story;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.annimon.stream.Stream;
import com.truyen.R;
import com.truyen.base.App;
import com.truyen.base.AppFragment;
import com.truyen.databinding.FragmentPageBinding;
import com.truyen.use_cases.chaps.ChapActivity;
import com.truyen.util.FileHandler;
import com.truyen.util.Log;

import java.io.File;
import java.util.Collections;
import java.util.List;

/**
 * MyApplication - com.truyen.use_cases.story
 * Created by Kerofrog on 7/11/17.
 */

public class PageFragment extends AppFragment {

    public static final int CHAP_PER_PAGE = 50;

    public static PageFragment newInstance(int page, String folder) {
        Log.e("FOLDER: " + folder);
        Bundle args = new Bundle();
        args.putString("folder", folder);
        args.putInt("page", page);
        PageFragment fragment = new PageFragment();
        fragment.setArguments(args);
        return fragment;
    }

    FragmentPageBinding binding;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_page, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        String s = getArguments().getString("folder", "");
        int page = getArguments().getInt("page", 0);
        File f = new File(s);
        if (!f.isDirectory()) {
            return;
        }
        int start = (getArguments().getInt("page", 1) - 1) * CHAP_PER_PAGE + 1;
        File[] files = f.listFiles((file, s1) -> {
            int first = s1.indexOf('.');
            if (first == -1) {
                return false;
            }
            String chap = s1.substring(0, first);
            try {
                int currentChap = Integer.parseInt(chap);
                return start <= currentChap && currentChap <= start + CHAP_PER_PAGE;
            } catch (Exception e) {
                return false;
            }
        });
        List<String> chapters = Stream.of(files)
                .map(t -> FileHandler.getFileName(t.getAbsolutePath()).replace(".txt", ""))
                .toList();
        Collections.sort(chapters, (s12, t1) -> {
            int first = s12.indexOf('.');
            int first2 = t1.indexOf('.');
            if (first == -1 || first2 == -1) {
                return 0;
            }
            String chap1 = s12.substring(0, first);
            String chap2 = t1.substring(0, first2);
            try {
                int currentChap = Integer.parseInt(chap1);
                int currentChap2 = Integer.parseInt(chap2);
                return currentChap - currentChap2;
            } catch (Exception ignored) {
            }
            return 0;
        });
        binding.listView.setAdapter(new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, chapters));
        binding.listView.setOnItemClickListener((adapterView, view, i, l) -> ChapActivity.start(getContext(), App.getStory(getContext()), 50 * (page - 1) + i + 1));
    }
}

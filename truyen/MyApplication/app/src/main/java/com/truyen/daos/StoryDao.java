package com.truyen.daos;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import android.support.annotation.Nullable;

import com.truyen.models.Story;

import java.util.List;

/**
 * MyApplication - com.truyen.daos
 * Created by Kerofrog on 7/10/17.
 */

@Dao
public interface StoryDao {
    @Query("SELECT * FROM STORY")
    List<Story> loadAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<Story> stories);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Story story);

    @Update
    void update(Story story);

    @Query("SELECT * FROM STORY WHERE id IN (SELECT storyId FROM CATEGORY_STORY WHERE categoryId = (:categoryId))")
    List<Story> loadStoryOfCategory(long categoryId);

    @Nullable
    @Query("SELECT * FROM STORY WHERE id = :id LIMIT 1")
    Story find(long id);
}

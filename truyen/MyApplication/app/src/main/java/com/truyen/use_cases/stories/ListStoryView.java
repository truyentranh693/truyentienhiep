package com.truyen.use_cases.stories;

import com.truyen.models.Category;
import com.truyen.models.Story;

import java.util.List;

/**
 * MyApplication - com.truyen.use_cases.stories
 * Created by Kerofrog on 7/10/17.
 */

public interface ListStoryView {
    void showLoadingCategory();
    void hideLoadingCategory();

    void showLoadingStory();
    void hideLoadingStory();

    void setCategories(List<Category> categories);
    void setStories(List<Story> stories);
    void updateDownloadProgress(int progress);
    void dismissDownloadProgress();
    void showDownloadProgress();

    void showDeleteAlert(Story story);

    void showError();
}

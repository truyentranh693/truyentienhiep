package com.truyen.use_cases.chaps;

/**
 * MyApplication - com.truyen.use_cases.chaps
 * Created by Kerofrog on 7/12/17.
 */

public interface OnReadChap {
    void read(int chap);
}

package com.truyen.use_cases.stories;

import android.support.annotation.Nullable;

import com.truyen.base.App;
import com.truyen.models.Category;
import com.truyen.models.Story;
import com.truyen.util.Log;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.Collections;

/**
 * MyApplication - com.truyen.use_cases.stories
 * Created by Kerofrog on 7/10/17.
 */

class ListStoryPresenter {

    @Nullable private
    ListStoryView view;
    private ListStoryModel model;

    ListStoryPresenter(@Nullable ListStoryView view, ListStoryModel model) {
        this.view = view;
        this.model = model;
    }

    void loadCategories() {
        if (view == null) {
            return;
        }
        view.showLoadingCategory();
        model.loadCategories(item -> {
            if (view != null) {
                Log.e("categories: " + item.size());
                view.setCategories(item);
                view.hideLoadingCategory();
            }
        }, throwable -> {
            Log.e(throwable);
            if (view != null) {
                view.hideLoadingCategory();
                view.showError();
            }
        });
    }

    void onCategorySelected(Category category) {
        Log.e("onCategory Selected");
        if (view != null) {
            view.showLoadingStory();
        }
        model.loadStoryFromCategory(category, item -> {
            Collections.sort(item, (story, t1) -> story.getName().compareTo(t1.getName()));
            if (view != null) {
                view.setStories(item);
                view.hideLoadingStory();
            }
        }, throwable -> {
            Log.e(throwable);
            if (view != null) {
                view.hideLoadingStory();
                view.showError();
            }
        });
    }

    void onDetach() {
        view = null;
    }

    void onDownloadOrDeleteStory(Story story) {
        if (view == null) {
            return;
        }
        if (!story.isDownloaded()) {
            download(story);
        } else {
            view.showDeleteAlert(story);
        }

    }

    private void download(Story story) {
        if (view != null) {
            view.showDownloadProgress();
        }
        model.download(story, item -> {
            if (view != null) {
                view.dismissDownloadProgress();
            }
            Log.e("Success: " + item.getAbsolutePath());
        }, (byteTransferred, total, progress) -> {
            if (view != null) {
                view.updateDownloadProgress(progress);
            }
        }, throwable -> {
            Log.e("onError: " + throwable);
            if (view != null) {
                view.dismissDownloadProgress();
                view.showError();
            }
        });
    }

    void onConfirmDeleteStory(Story story) {
        story.setDownloadProgress(0);
        story.setState(Story.State.NORMAL);
        new Thread(() -> {
            App.database.getStoryDao().update(story);
            try {
                FileUtils.deleteDirectory(new File(story.getFolder()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }
}

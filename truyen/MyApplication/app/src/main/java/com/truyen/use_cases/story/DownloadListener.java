package com.truyen.use_cases.story;

/**
 * MyApplication - com.truyen.use_cases.story
 * Created by Kerofrog on 7/13/17.
 */

public interface DownloadListener {
    void onDownloaded();
}

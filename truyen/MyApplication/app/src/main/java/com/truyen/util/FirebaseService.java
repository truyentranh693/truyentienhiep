package com.truyen.util;

import android.os.Handler;
import android.os.Looper;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.orhanobut.logger.Logger;
import com.truyen.base.App;
import com.truyen.interfaces.Completion;
import com.truyen.interfaces.OnError;
import com.truyen.models.Story;

import java.io.File;

/**
 * MyApplication - com.truyen.util
 * Created by Kerofrog on 7/10/17.
 */

public class FirebaseService {
    public static final String CATEGORIES_FILE_NAME = "list.json";

    public static void readFile(String filePath, final Completion<String> completion, final OnError error) {
        downloadFile(filePath, App.folderPath + "temp.json", item -> {
            try {
                String content = FileHandler.readFile(item);
                completion.onSuccess(content);
            } catch (Exception e) {
                error.onError(e);
            }
        }, error);
    }

    private static void downloadFile(String filePath, String localPath, final Completion<File> completion, final OnError error) {



        Handler handler = new Handler(Looper.getMainLooper());
        if (filePath == null || filePath.isEmpty()) {
            handler.post(() -> error.onError(new Exception("Có lỗi xảy ra.")));
            return;
        }

        final File file = new File(localPath);
        FirebaseStorage.getInstance().setMaxDownloadRetryTimeMillis(10000);
        FirebaseStorage.getInstance()
                .getReference().child(filePath)
                .getFile(file)
                .addOnCompleteListener(task -> {
                    try {
                        handler.post(() -> completion.onSuccess(file));
                    } catch (Exception e) {
                        handler.post(() -> error.onError(e));
                    }
                })
                .addOnFailureListener(e -> {
                    Logger.e("onFailure download__" + filePath + "__: " + e);
                    handler.post(() -> error.onError(e));
                });
    }

    public static void incDownloadCount(Story story) {
        DatabaseReference ref = FirebaseDatabase.getInstance()
                .getReference(story.getName())
                .child("download");
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Object v = dataSnapshot.getValue();
                if (v == null) {
                    ref.setValue(1);
                } else if (v instanceof Long) {
                    ref.setValue(((long) v) + 1);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}

package com.truyen.models;

import android.arch.persistence.room.Entity;

/**
 * MyApplication - com.truyen.models
 * Created by Kerofrog on 7/10/17.
 */

@Entity(primaryKeys = {"categoryId", "storyId"}, tableName = "CATEGORY_STORY")
public class CategoryStory {

    private long categoryId;
    private long storyId;

    public CategoryStory(long categoryId, long storyId) {
        this.categoryId = categoryId;
        this.storyId = storyId;
    }

    public long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(long categoryId) {
        this.categoryId = categoryId;
    }

    public long getStoryId() {
        return storyId;
    }

    public void setStoryId(long storyId) {
        this.storyId = storyId;
    }
}

package com.truyen.interfaces;

/**
 * MyApplication - com.truyen.interfaces
 * Created by Kerofrog on 7/10/17.
 */

public interface OnProgress {
    void onProgress(long byteTransferred, long total, int progress);
}

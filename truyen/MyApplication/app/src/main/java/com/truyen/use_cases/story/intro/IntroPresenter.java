package com.truyen.use_cases.story.intro;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.truyen.base.App;
import com.truyen.interfaces.Completion;
import com.truyen.interfaces.OnError;
import com.truyen.interfaces.OnProgress;
import com.truyen.models.Story;
import com.truyen.util.FileHandler;
import com.truyen.util.FirebaseService;
import com.truyen.util.Log;
import com.truyen.util.ZipHelper;

import java.io.File;

/**
 * MyApplication - com.truyen.use_cases.story.intro
 * Created by Kerofrog on 7/11/17.
 */

class IntroPresenter {

    @Nullable
    private IntroView view;
    private Story story;

    IntroPresenter(@Nullable IntroView view, Story story) {
        this.view = view;
        this.story = story;
    }

    void onDownloadClick() {
        if (view == null) {
            return;
        }
        view.showDownloadProgress();
        download(story, item -> {
            if (view != null) {
                view.dismissDownloadProgress();
                view.refreshActivity();
            }
        }, (byteTransferred, total, progress) -> {
            if (view != null) {
                view.updateDownloadProgress(progress);
            }
        }, throwable -> {
            if (view != null) {
                view.dismissDownloadProgress();
                view.showErrorDownload("" + throwable);
            }
        });
    }

    void onLikeClick() {
        story.setLiked(!story.isLiked());
        if (view != null) {
            refreshLike(view);
        }
        new Thread(() -> App.database.getStoryDao().update(story)).start();
        final DatabaseReference ref = FirebaseDatabase.getInstance()
                .getReference(story.getName());
        DatabaseReference r = ref.child("likes").child("count");
        r.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() == null) {
                    r.setValue(1);
                } else if (dataSnapshot.getValue() instanceof Long) {
                    long count = (long) dataSnapshot.getValue();
                    if (story.isLiked()) {
                        r.setValue(count + 1);
                    } else {
                        r.setValue(count - 1);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("onCancelled: " + databaseError);

            }
        });
    }

    void onReadClick() {
        if (view == null) {
            return;
        }
        if (story.getCurrentChapter() > 0) {
            view.showContinueReadingAlert();
        } else {
            view.readChap(story, 1);
        }
    }

    void onContinueReadingCancel() {
        if (view != null) {
            view.readChap(story, 1);
        }
    }

    void onContinueReadingOk() {
        if (view != null) {
            view.readChap(story, story.getCurrentChapter());
        }
    }

    private void download(Story story, Completion<File> completion, OnProgress progress, OnError onError) {
        final File file = new File(App.folderPath + story.getZip());
        boolean success = FileHandler.createFolderForFilePath(file.getAbsolutePath());
        if (!success) {
            onError.onError(new Exception("Cannot create file " + file.getAbsolutePath()));
            return;
        }
        FirebaseStorage.getInstance()
                .getReference().child(story.getZip())
                .getFile(file)
                .addOnCompleteListener(task -> {
                    if (!task.isSuccessful()) {
                        return;
                    }
                    FirebaseService.incDownloadCount(story);
                    try {
                        ZipHelper.unzip2(file.getAbsolutePath(), App.folderPath);
                        story.setState(Story.State.DOWNLOADED);
                        completion.onSuccess(file);
                        new Thread(() -> App.database.getStoryDao().update(story)).start();
                    } catch (Exception e) {
                        onError.onError(e);
                    }
                })
                .addOnProgressListener(taskSnapshot -> {
                    int prog = (int) (taskSnapshot.getBytesTransferred() * 1.0f / taskSnapshot.getTotalByteCount() * 100);
                    story.setDownloadProgress(prog);
                    new Thread(() -> App.database.getStoryDao().update(story)).start();
                    progress.onProgress(taskSnapshot.getBytesTransferred(), taskSnapshot.getTotalByteCount(), prog);
                })
                .addOnFailureListener(e -> {
                    story.setDownloadProgress(0);
                    new Thread(() -> App.database.getStoryDao().update(story)).start();
                    onError.onError(e);
                });

    }

    void attach() {
        if (view == null) {
            return;
        }
        if (story.isDownloaded()) {
            view.showDownloadDisable();
            view.showReadingEnable();
        } else {
            view.showDownloadEnable();
            view.showReadingDisable();
        }
        refreshLike(view);
    }

    private void refreshLike(@NonNull IntroView view) {
        if (story.isLiked()) {
            view.showLikeEnabled();
        } else {
            view.showLikeNormal();
        }
    }

    void detach() {
        view = null;
    }
}

package com.truyen.util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * TruyenTienHiep - com.devteam.truyentienhiep.services
 * Created by Kerofrog on 7/8/17.
 */

public class ZipHelper {

    public static boolean unzip2(String zipPath, String destinationFolder) {
        if (!new File(zipPath).exists()) {
            return false;
        }
        // if path is not zip file, return false
        if (!zipPath.endsWith(".zip") || !new File(zipPath).exists()) {
            //            Logger.e("NOT A ZIP FILE");
            return false;
        }

        if (!destinationFolder.endsWith("/")) {
            destinationFolder += "/";
        }

        // create destination folder if not exist
        File destination = new File(destinationFolder);
        if (!destination.exists()) {
            destination.mkdir();
        }

        InputStream is;
        ZipInputStream zis;
        try {
            String filename;
            is = new FileInputStream(zipPath);
            zis = new ZipInputStream(new BufferedInputStream(is));
            ZipEntry ze;
            byte[] buffer = new byte[1024];
            int count;

            while ((ze = zis.getNextEntry()) != null) {
                filename = ze.getName();
                if (ze.isDirectory()) {
                    File fmd = new File(destinationFolder + filename);
                    fmd.mkdirs();
                    continue;
                }

                FileOutputStream fos = new FileOutputStream(destinationFolder + filename);

                while ((count = zis.read(buffer)) != -1) {
                    fos.write(buffer, 0, count);
                }
                new File(zipPath).delete();
                fos.close();
                zis.closeEntry();
            }

            zis.close();
            //            Logger.e("UNZIP SUCCESS");
            return true;
        } catch (IOException e) {
            //Logger.e("UNZIP FAIL");
            return false;
        }
    }


}

package com.truyen.base;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.truyen.daos.CategoryDao;
import com.truyen.daos.CategoryStoryDao;
import com.truyen.daos.StoryDao;
import com.truyen.models.Category;
import com.truyen.models.CategoryStory;
import com.truyen.models.Story;

/**
 * MyApplication - com.truyen.base
 * Created by Kerofrog on 7/10/17.
 */

@Database(entities = {Category.class, Story.class, CategoryStory.class}, version = 3, exportSchema = false)
public abstract class RoomDb extends RoomDatabase {

    public abstract CategoryDao getCategoryDao();
    public abstract StoryDao getStoryDao();
    public abstract CategoryStoryDao getCategoryStoryDao();

}

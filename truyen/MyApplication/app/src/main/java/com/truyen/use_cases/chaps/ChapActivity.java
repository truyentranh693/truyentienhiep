package com.truyen.use_cases.chaps;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdSettings;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.firebase.database.FirebaseDatabase;
import com.orhanobut.logger.Logger;
import com.truyen.R;
import com.truyen.base.App;
import com.truyen.base.AppActivity;
import com.truyen.databinding.ActivityChapBinding;
import com.truyen.models.Story;

import java.io.File;

/**
 * MyApplication - com.truyen.use_cases.chaps
 * Created by Kerofrog on 7/12/17.
 */

public class ChapActivity extends AppActivity implements OnReadChap {


    public static void start(Context c, Story story, int page) {
        Intent i = new Intent(c, ChapActivity.class);
        i.putExtra("page", page);
        c.startActivity(i);
    }

    ActivityChapBinding binding;
    Story story;

    AdView adView;

    com.facebook.ads.AdView adViewFb;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_chap);
        story = App.getStory(this);
        int chap = getIntent().getIntExtra("page", 1) - 1;
        binding.viewPager.setAdapter(new ChapPager(getSupportFragmentManager(), story));
        binding.viewPager.setCurrentItem(chap);
        setupToolbar(story);

        //        setupAdViewGG();
        setupAdViewFb();


    }


    void setupAdViewFb() {

        AdSettings.addTestDevice("c67e19659c33fe0ac6eb67f5fd185b33");
        adViewFb = new com.facebook.ads.AdView(this, "887270051427573_889447657876479", com.facebook.ads.AdSize.BANNER_HEIGHT_50);
        adViewFb.setAdListener(new com.facebook.ads.AdListener() {
            @Override
            public void onError(Ad ad, AdError adError) {
                Logger.e("Error: " + adError.getErrorMessage());
            }

            @Override
            public void onAdLoaded(Ad ad) {
                Logger.e("onAdLoaded");
            }

            @Override
            public void onAdClicked(Ad ad) {
                Logger.e("onAdClicked");

            }

            @Override
            public void onLoggingImpression(Ad ad) {
                Logger.e("onLoggingImpression");

            }
        });

        binding.adContainer.addView(adViewFb);
        adViewFb.loadAd();
    }

    void setupAdViewGG() {
        adView = new AdView(this);
        adView.setAdSize(AdSize.BANNER);
        adView.setAdUnitId("ca-app-pub-6348323568814805/1555248970");
        binding.adContainer.addView(adView);
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice("D2A64ECB759A3A88A145EEBCF84465FB")
                //                .addTestDevice("3a2cfbcb26914f4590469dcad32b9006")
                .build();
        adView.loadAd(adRequest);
        adView.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
            }

            @Override
            public void onAdFailedToLoad(int i) {
                Logger.e("onAdFailedToLoad " + i);
            }

            @Override
            public void onAdLeftApplication() {
                Logger.e("onAdLeftApplication");
            }

            @Override
            public void onAdOpened() {
                Logger.e("onAdOpened");
            }

            @Override
            public void onAdLoaded() {
                Logger.e("onAdLoaded");
            }
        });
    }

    void setupToolbar(Story story) {
        setTitle(story.getName());
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setElevation(0);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return true;
    }

    @Override
    protected void onDestroy() {
        if (adView != null) {
            adView.destroy();
        }
        if (adViewFb != null) {
            adViewFb.destroy();
        }
        super.onDestroy();
    }

    @Override
    public void read(int chap) {
        story.setCurrentChapter(chap);
        new Thread(() -> App.database.getStoryDao().update(story)).start();
    }

    private class ChapPager extends FragmentPagerAdapter {

        Story story;

        ChapPager(FragmentManager fm, Story story) {
            super(fm);
            this.story = story;
        }

        @Override
        public Fragment getItem(int position) {
            File f = new File(story.getFolder());
            File[] files = f.listFiles((file, s1) -> {
                int first = s1.indexOf('.');
                if (first == -1) {
                    return false;
                }
                String chap = s1.substring(0, first);
                try {
                    int currentChap = Integer.parseInt(chap);
                    return currentChap == position + 1;
                } catch (Exception e) {
                    return false;
                }
            });
            if (files == null) {
                FirebaseDatabase.getInstance()
                        .getReference("bugs")
                        .child("ChapActivity.getItem")
                        .setValue(story);
                throw new RuntimeException("Unknown exception");
            }
            String path = null;
            if (files.length > 0) {
                path = files[0].getAbsolutePath();
            }
            return ChapFragment.newInstance(path, position);
        }

        @Override
        public int getCount() {
            return story.getChapterCount();
        }
    }
}
//

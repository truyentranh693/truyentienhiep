package com.truyen.daos;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;

import com.truyen.models.CategoryStory;

import java.util.List;

/**
 * MyApplication - com.truyen.daos
 * Created by Kerofrog on 7/10/17.
 */

@Dao
public interface CategoryStoryDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<CategoryStory> categoryStoryList);

}

package com.truyen.use_cases.story.intro;

import com.truyen.models.Story;

/**
 * MyApplication - com.truyen.use_cases.story.intro
 * Created by Kerofrog on 7/11/17.
 */

interface IntroView {

    void showLikeNormal();
    void showLikeEnabled();
    void showReadingDisable();
    void showReadingEnable();
    void showDownloadDisable();
    void showDownloadEnable();

    void showContinueReadingAlert();
    void showDownloadProgress();
    void updateDownloadProgress(int progress);
    void dismissDownloadProgress();
    void showErrorDownload(String error);
    void readChap(Story story, int chapter);
    void refreshActivity();
}

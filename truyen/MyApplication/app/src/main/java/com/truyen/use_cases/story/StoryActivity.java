package com.truyen.use_cases.story;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;

import com.truyen.R;
import com.truyen.base.App;
import com.truyen.base.AppActivity;
import com.truyen.databinding.ActivityStoryDetailBinding;
import com.truyen.models.Story;
import com.truyen.use_cases.story.intro.IntroFragment;
import com.truyen.util.Log;
import com.truyen.util.ResUtil;

/**
 * MyApplication - com.truyen.use_cases.story
 * Created by Kerofrog on 7/11/17.
 */

public class StoryActivity extends AppActivity implements DownloadListener {

    public static void start(Context c, Story story) {
        Log.e(story.toString());
        Intent i = new Intent(c, StoryActivity.class);
        App.setStory(c, story);
        c.startActivity(i);
    }

    ActivityStoryDetailBinding binding;
    StoryPagerAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_story_detail);

        Story story = App.getStory(this);
        Log.e(story.toString());
        binding.viewPager.setAdapter(adapter = new StoryPagerAdapter(getSupportFragmentManager(), story));
        binding.tabLayout.setupWithViewPager(binding.viewPager);
        setupToolbar(story);
    }

    void setupToolbar(Story story) {
        setTitle(story.getName());
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setElevation(0);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return true;
    }

    @Override
    public void onDownloaded() {
        adapter.refresh();
    }


    private class StoryPagerAdapter extends FragmentPagerAdapter {

        Story story;
        int numberOfPage = 0;

        StoryPagerAdapter(FragmentManager fm, Story story) {
            super(fm);
            this.story = story;
            if (story.isDownloaded()) {
                numberOfPage = (int) Math.ceil(story.getChapterCount() / 50.0f);
            }
        }

        void refresh() {
            if (story.isDownloaded()) {
                numberOfPage = (int) Math.ceil(story.getChapterCount() / 50.0f);
            }
            notifyDataSetChanged();
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                return IntroFragment.newInstance(story);
            }
            return PageFragment.newInstance(position, story.getFolder());
        }

        @Override
        public int getCount() {
            return numberOfPage + 1;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if (position == 0) {
                return ResUtil.getInstance().getString(R.string.intro);
            } else {
                return ResUtil.getInstance().getString(R.string.page, position);
            }
        }
    }

}

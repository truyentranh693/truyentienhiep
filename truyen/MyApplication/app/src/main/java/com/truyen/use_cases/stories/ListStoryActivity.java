package com.truyen.use_cases.stories;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.databinding.library.baseAdapters.BR;
import com.truyen.R;
import com.truyen.base.RecyclerAdapter;
import com.truyen.databinding.ActivityListStoryBinding;
import com.truyen.databinding.RowStoryBinding;
import com.truyen.interfaces.OnDownloadStoryClickListener;
import com.truyen.models.Category;
import com.truyen.models.Story;
import com.truyen.use_cases.story.StoryActivity;
import com.truyen.util.ResUtil;

import java.util.List;

public class ListStoryActivity extends AppCompatActivity implements ListStoryView {

    ListStoryPresenter presenter;
    ActivityListStoryBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_list_story);
        presenter = new ListStoryPresenter(this, new ListStoryModel(this));
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        binding.recyclerView.addItemDecoration(new ItemDecorator());
        presenter.loadCategories();
        binding.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long id) {
                Object obj = adapterView.getItemAtPosition(i);
                if (obj instanceof Category) {
                    presenter.onCategorySelected((Category) obj);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        int count = getSharedPreferences("count", MODE_PRIVATE).getInt("count", 0);
        if (count != 0) {
            if (count % 10 == 0) {
                showRatingDialog();
            }
            getSharedPreferences("count", MODE_PRIVATE).edit().putInt("count", count + 1).apply();
        } else {
            getSharedPreferences("count", MODE_PRIVATE).edit().putInt("count", 1).apply();
        }


    }

    void showRatingDialog() {
        new AlertDialog.Builder(this)
                .setTitle("Đánh giá")
                .setMessage("Giúp đánh giá ứng dụng của chúng tôi để cải thiện chất lượng ứng dụng")
                .setNegativeButton("Không, cảm ơn", null)
                .setPositiveButton("Đánh giá", (dialogInterface, i) -> {
                    final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                    }
                }).show();
    }

    ProgressDialog categoryDialog;

    @Override
    public void showLoadingCategory() {
        categoryDialog = ProgressDialog.show(this, ResUtil.getInstance().getString(R.string.downloading), getString(R.string.loading_story), true, true);
    }

    @Override
    public void hideLoadingCategory() {
        if (categoryDialog != null) {
            categoryDialog.dismiss();
        }
    }

    ProgressDialog storyDialog;

    @Override
    public void showLoadingStory() {
        storyDialog = ProgressDialog.show(this, ResUtil.getInstance().getString(R.string.downloading), getString(R.string.loading_story), true, true);
    }

    @Override
    public void hideLoadingStory() {
        if (storyDialog != null) {
            storyDialog.dismiss();
        }
    }

    @Override
    public void setCategories(List<Category> categories) {
        binding.spinner.setAdapter(new CategoryAdapter(this, categories));
    }

    @Override
    public void setStories(List<Story> stories) {
        StoryAdapter<Story> adapter = new StoryAdapter<>(this, stories, BR.story, R.layout.row_story);
        adapter.setListener(story -> presenter.onDownloadOrDeleteStory(story));
        adapter.setOnItemClickListener(item -> StoryActivity.start(this, item));
        binding.recyclerView.setAdapter(adapter);
    }

    ProgressDialog downloadProgressDialog;

    @Override
    public void showDownloadProgress() {
        downloadProgressDialog = new ProgressDialog(this);
        downloadProgressDialog.setCancelable(false);
        downloadProgressDialog.setTitle(getString(R.string.downloading));
        downloadProgressDialog.setIndeterminate(false);
        downloadProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        downloadProgressDialog.show();
    }

    @Override
    public void updateDownloadProgress(int progress) {
        if (downloadProgressDialog != null && downloadProgressDialog.isShowing()) {
            downloadProgressDialog.setProgress(progress);
        }
    }


    @Override
    public void dismissDownloadProgress() {
        if (downloadProgressDialog != null && downloadProgressDialog.isShowing()) {
            downloadProgressDialog.dismiss();
        }
    }

    @Override
    public void showDeleteAlert(Story story) {
        new AlertDialog.Builder(this)
                .setTitle(R.string.delete_story)
                .setMessage(ResUtil.getInstance().getString(R.string.delete_story_description, story.getName()))
                .setPositiveButton(R.string.delete, (dialogInterface, i) -> presenter.onConfirmDeleteStory(story))
                .setNegativeButton(R.string.cancel, null)
                .show();
    }

    @Override
    public void showError() {
        new AlertDialog.Builder(this)
                .setTitle(R.string.error_occur)
                .setMessage(ResUtil.getInstance().getString(R.string.no_internet))
                .setPositiveButton(R.string.ok, null)
                .setCancelable(true)
                .show();
    }

    @Override
    protected void onDestroy() {
        presenter.onDetach();
        super.onDestroy();
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    private static class ItemDecorator extends RecyclerView.ItemDecoration {
        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int spacing = ResUtil.dpToPx(8);
            if (parent.getChildAdapterPosition(view) == 0) {
                outRect.top = spacing;
            } else {
                outRect.top = spacing / 2;
            }
            outRect.left = outRect.right = outRect.bottom = spacing / 2;
        }
    }

    private static class CategoryAdapter extends BaseAdapter {
        List<Category> categories;
        LayoutInflater inflater;

        CategoryAdapter(Context context, List<Category> categories) {
            this.categories = categories;
            inflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return categories.size();
        }

        @Override
        public Object getItem(int i) {
            return categories.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            if (view == null) {
                view = inflater.inflate(R.layout.row_category, viewGroup, false);
            }
            ((TextView) view.findViewById(R.id.textView)).setText(categories.get(i).getType());
            return view;
        }

    }

    private static class StoryAdapter<T> extends RecyclerAdapter<T> {

        OnDownloadStoryClickListener listener;

        StoryAdapter(Context context, List<T> items, int variableID, @LayoutRes int layout) {
            super(context, items, variableID, layout);
        }

        public void setListener(OnDownloadStoryClickListener listener) {
            this.listener = listener;
        }

        @Override
        public void onBindViewHolder(BindingHolder holder, int position) {
            super.onBindViewHolder(holder, position);
            if (holder.getBinding() instanceof RowStoryBinding) {
                RowStoryBinding binding = (RowStoryBinding) holder.getBinding();
                binding.iconDownload.setOnClickListener(view -> {
                            if (listener != null) {
                                listener.onClick((Story) getItem(position));
                            }
                        }
                );
            }
        }
    }
}

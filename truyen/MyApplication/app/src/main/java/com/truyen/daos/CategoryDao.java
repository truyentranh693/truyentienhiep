package com.truyen.daos;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.truyen.models.Category;

import java.util.List;

/**
 * MyApplication - com.truyen.daos
 * Created by Kerofrog on 7/10/17.
 */

@Dao
public interface CategoryDao {

    @Query("SELECT * FROM CATEGORY")
    List<Category> loadAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<Category> categories);

    @Update
    void update(Category category);

}

package com.truyen.base;

import android.app.Application;
import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.migration.Migration;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.multidex.MultiDex;

import com.crashlytics.android.Crashlytics;
import com.facebook.stetho.Stetho;
import com.google.android.gms.ads.MobileAds;
import com.google.gson.Gson;
import com.truyen.models.Story;
import com.truyen.util.ResUtil;

import java.util.UUID;

import io.fabric.sdk.android.Fabric;

/**
 * MyApplication - com.truyen.base
 * Created by Kerofrog on 7/10/17.
 */

public class App extends Application {

    public static String folderPath = "";
    public static RoomDb database;

    private static Story story;

    public static Story getStory(Context context) {
        if (story == null) {
            String json = context.getSharedPreferences("pref", MODE_PRIVATE)
                    .getString("story", "");
            story = new Gson().fromJson(json, Story.class);
        }
        return story;
    }

    public static void setStory(Context context, @NonNull Story story) {
        context.getSharedPreferences("pref", MODE_PRIVATE)
                .edit().putString("story", new Gson().toJson(story)).apply();
        App.story = story;
    }

    public static String getUid(Context context) {
        String json = context.getSharedPreferences("pref", MODE_PRIVATE)
                .getString("uid", null);
        if (json == null) {
            json = UUID.randomUUID().toString();
            context.getSharedPreferences("pref", MODE_PRIVATE).edit()
                    .putString("uid", json).apply();
        }
        return json;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        folderPath = getFilesDir().getAbsolutePath();
        //Environment.getExternalStorageDirectory() + "/Download"; //
        if (!folderPath.endsWith("/")) {
            folderPath += "/";
        }
        ResUtil.getInstance().init(this);
        database = Room.databaseBuilder(this, RoomDb.class, "database")
                .addMigrations(new Migrate2_3(2, 3))
                .build();
        Stetho.initializeWithDefaults(this);
        MultiDex.install(this);
        MobileAds.initialize(this, "ca-app-pub-6348323568814805~4648316177");
    }

    private static class Migrate2_3 extends Migration {

        /**
         * Creates a new migration between {@code startVersion} and {@code endVersion}.
         *
         * @param startVersion The start version of the database.
         * @param endVersion   The end version of the database after this migration is applied.
         */
        public Migrate2_3(int startVersion, int endVersion) {
            super(startVersion, endVersion);
        }

        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE Category ADD COLUMN updatedAt INTEGER");
            database.execSQL("ALTER TABLE Category ADD COLUMN storyUpdatedAt INTEGER");


            database.execSQL("ALTER TABLE Story ADD COLUMN updatedAt INTEGER");
            database.execSQL("ALTER TABLE Story ADD COLUMN folderName TEXT");
        }
    }

}

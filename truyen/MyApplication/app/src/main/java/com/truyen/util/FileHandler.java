package com.truyen.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

/**
 * TruyenTienHiep - com.devteam.truyentienhiep.services
 * Created by Kerofrog on 7/9/17.
 */

public class FileHandler {

    public static String readFile(File file) throws Exception {
        //Read text from file
        StringBuilder text = new StringBuilder();
        BufferedReader br = new BufferedReader(new FileReader(file));
        String line;

        while ((line = br.readLine()) != null) {
            text.append(line);
            text.append('\n');
        }
        br.close();
        return text.toString();
    }

    public static String getFileName(String filePath) {
        int last = filePath.lastIndexOf('/');
        if (last == -1) {
            return filePath;
        }
        return filePath.substring(last + 1);
    }

    public static boolean createFolderForFilePath(String filePath) {
        int last = filePath.lastIndexOf('/');
        if (last == 1) {
            return false;
        }
        String folderPath = filePath.substring(0, last);
        File f = new File(folderPath);
        return f.exists() || f.mkdirs();
    }
}

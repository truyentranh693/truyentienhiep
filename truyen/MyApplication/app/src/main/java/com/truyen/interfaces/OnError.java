package com.truyen.interfaces;

/**
 * MyApplication - com.truyen.interfaces
 * Created by Kerofrog on 7/10/17.
 */

public interface OnError {

    void onError(Throwable throwable);

}

package com.truyen.util;

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.ArrayRes;
import android.support.annotation.StringRes;

/**
 * ProjectManagement - com.devteam.pm.projectmanagement.utils
 * Created by Kerofrog on 5/28/17.
 */

public class ResUtil {

    private static ResUtil instance;
    private Context context;
    private ResUtil() {
    }

    public static ResUtil getInstance() {
        if (instance == null) {
            instance = new ResUtil();
        }
        return instance;
    }

    public int drawable(String name) {
        return context.getResources().getIdentifier(name, "drawable", context.getPackageName());
    }


    public  void init(Context context) {
        this.context = context.getApplicationContext();
    }

    public String getString(@StringRes int id, Object... params) {
        return context.getString(id, params);
    }

    public String getString(String name, Object... params) {
        return getString(context.getResources().getIdentifier(name, "string", context.getPackageName()), params);
    }


    public static int pxToDp(int px) {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }

    public static int screenWidthDp() {
        return pxToDp(screenWidth());
    }

    public static int screenWidth() {
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    public static int screenHeight() {
        return Resources.getSystem().getDisplayMetrics().heightPixels;
    }

    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public String[] getStringArray(@ArrayRes int id) {
        return context.getResources().getStringArray(id);
    }

    //    public Size getImageSize(String filePath) {
    //        BitmapFactory.Options options = new BitmapFactory.Options();
    //        options.inJustDecodeBounds = true;
    //        BitmapFactory.decodeFile(filePath, options);
    //        int imageHeight = options.outHeight;
    //        int imageWidth = options.outWidth;
    //        return new Size(imageWidth, imageHeight);
    //    }
    //
    //    public Size getImageSize(@DrawableRes int image) {
    //        BitmapFactory.Options options = new BitmapFactory.Options();
    //        options.inJustDecodeBounds = true;
    //        BitmapFactory.decodeResource(context.getResources(), image);
    //        int imageHeight = options.outHeight;
    //        int imageWidth = options.outWidth;
    //        return new Size(imageWidth, imageHeight);
    //    }

    //    public static int getNeededHeight(int w1, int h1, int w2) {
    //        return h1 * w2 / w1;
    //    }
    //
    //    public static int getNeededHeight(Size size, int w) {
    //        return size.getHeight() * w / size.getWidth();
    //    }
}

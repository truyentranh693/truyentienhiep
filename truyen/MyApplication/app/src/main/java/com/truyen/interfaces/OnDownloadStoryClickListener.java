package com.truyen.interfaces;

import com.truyen.models.Story;

/**
 * MyApplication - com.truyen.interfaces
 * Created by Kerofrog on 7/10/17.
 */

public interface OnDownloadStoryClickListener {

    void onClick(Story story);

}

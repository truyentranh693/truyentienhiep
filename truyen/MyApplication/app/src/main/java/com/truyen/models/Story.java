package com.truyen.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.databinding.Bindable;
import android.databinding.Observable;
import android.databinding.PropertyChangeRegistry;
import android.support.annotation.IntDef;
import android.support.annotation.StringDef;

import com.annimon.stream.Optional;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.FirebaseDatabase;
import com.truyen.BR;
import com.truyen.base.App;
import com.truyen.util.MapUtil;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Map;

/**
 * MyApplication - com.truyen.models
 * Created by Kerofrog on 7/10/17.
 */

@Entity
public class Story implements Observable {

    private Story(Builder builder) {
        setId(builder.id);
        setName(builder.name);
        setDescription(builder.description);
        setZip(builder.zip);
        setImage(builder.image);
        setType(builder.type);
        setAuthor(builder.author);
        setChapterCount(builder.chapterCount);
        setFolderName(builder.folderName);
    }

    public Story(long id, String name, String description, String zip, String image, String type, String author, int chapterCount, int versionCode, String storageType, String folderName, int currentChapter, boolean isLiked, int state, int downloadProgress) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.zip = zip;
        this.image = image;
        this.type = type;
        this.author = author;
        this.chapterCount = chapterCount;
        this.versionCode = versionCode;
        this.storageType = storageType;
        this.folderName = folderName;
        this.currentChapter = currentChapter;
        this.isLiked = isLiked;
        this.state = state;
        this.downloadProgress = downloadProgress;
    }

    @Retention(RetentionPolicy.SOURCE)
    public
    @IntDef({
            State.NEED_UPDATED, State.NORMAL, State.DOWNLOADED
    })
    @interface State {
        int NORMAL = 0;
        int DOWNLOADED = 1;
        int NEED_UPDATED = 2;
    }

    @Override
    public void addOnPropertyChangedCallback(OnPropertyChangedCallback callback) {
        registry.add(callback);
    }

    @Override
    public void removeOnPropertyChangedCallback(OnPropertyChangedCallback callback) {
        registry.remove(callback);

    }

    @Retention(RetentionPolicy.SOURCE)
    @StringDef({StorageType.FB_STORAGE})
    @interface StorageType {
        String FB_STORAGE = "firebase_storage";
    }

    @Ignore
    private transient PropertyChangeRegistry registry = new PropertyChangeRegistry();

    @PrimaryKey
    private long id;
    private String name;
    private String description;
    private String zip;
    private String image;
    private String type;
    private String author;
    private int chapterCount;
    private int versionCode;
    private String storageType;
    private String folderName;
    private int currentChapter;
    private boolean isLiked;
    private int state;
    private long updatedAt;

    public long getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(long updatedAt) {
        this.updatedAt = updatedAt;
    }

    public PropertyChangeRegistry getRegistry() {
        return registry;
    }

    public void setRegistry(PropertyChangeRegistry registry) {
        this.registry = registry;
    }

    public String getFolderName() {
        return folderName;
    }

    public void setFolderName(String folderName) {
        this.folderName = folderName;
    }

    public boolean isLiked() {
        return isLiked;
    }

    public void setLiked(boolean liked) {
        isLiked = liked;
    }

    public void setStorageType(String storageType) {
        this.storageType = storageType;
    }

    public String getStorageType() {
        return storageType;
    }

    @Bindable
    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
        registry.notifyChange(this, BR.state);
    }

    public boolean isFirebaseStorage() {
        return storageType.equalsIgnoreCase(StorageType.FB_STORAGE);
    }


    public int getCurrentChapter() {
        return currentChapter;
    }

    public void setCurrentChapter(int currentChapter) {
        this.currentChapter = currentChapter;
    }

    public boolean isDownloaded() {
        return state == State.DOWNLOADED;
    }

    @Bindable
    public int getDownloadProgress() {
        return downloadProgress;
    }

    public void setDownloadProgress(int downloadProgress) {
        this.downloadProgress = downloadProgress;
        registry.notifyChange(this, BR.downloadProgress);
    }

    private int downloadProgress = -1;

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getChapterCount() {
        return chapterCount;
    }

    public void setChapterCount(int chapterCount) {
        this.chapterCount = chapterCount;
    }

    public int getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(int versionCode) {
        this.versionCode = versionCode;
    }

    public String getFolder() {
        if (zip == null) {
            FirebaseDatabase.getInstance()
                    .getReference("bugs")
                    .child("Story/getFolder")
                    .setValue(this);
            throw new RuntimeException("Error cmnr");
        }
        int last = zip.lastIndexOf('/');
        if (last == -1) {
            return App.folderPath + zip.replace(".zip", "") + "/";
        }
        return App.folderPath + zip.substring(last + 1).replace(".zip", "") + "/";
    }

    @Override
    public String toString() {
        return "Story{" +
                "hash=" + hashCode() +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", zip='" + zip + '\'' +
                ", image='" + image + '\'' +
                ", type='" + type + '\'' +
                ", author='" + author + '\'' +
                ", chapterCount=" + chapterCount +
                ", versionCode=" + versionCode +
                ", isLike=" + isLiked +
                '}';
    }

    public static Optional<Story> fromDatasnapshot(DataSnapshot dataSnapshot) {
        try {
            MapUtil m = MapUtil.of((Map<String, Object>) (dataSnapshot.getValue()));
            return Optional.of(new Builder()
                    .id(m.getLong("id"))
                    .name(m.getString("name"))
                    .description(m.getString("description"))
                    .zip(m.getString("zip"))
                    .image(m.getString("image"))
                    .type(m.getString("type"))
                    .author(m.getString("author"))
                    .chapterCount(m.getInt("chapterCount"))
                    .folderName(m.getString("folderName"))
                    .updatedAt(m.getLong("updatedAt"))
                    .build());
        } catch (Exception e) {

        }
        return Optional.empty();
    }

    public static final class Builder {
        private long id;
        private String name;
        private String description;
        private String zip;
        private String image;
        private String type;
        private String author;
        private int chapterCount;
        private String folderName;
        private long updatedAt;

        public Builder() {
        }

        public Builder updatedAt(long val) {
            updatedAt = val;
            return this;
        }

        public Builder id(long val) {
            id = val;
            return this;
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public Builder description(String val) {
            description = val;
            return this;
        }

        public Builder zip(String val) {
            zip = val;
            return this;
        }

        public Builder image(String val) {
            image = val;
            return this;
        }

        public Builder type(String val) {
            type = val;
            return this;
        }

        public Builder author(String val) {
            author = val;
            return this;
        }

        public Builder chapterCount(int val) {
            chapterCount = val;
            return this;
        }

        public Builder folderName(String val) {
            folderName = val;
            return this;
        }

        public Story build() {
            return new Story(this);
        }
    }
}

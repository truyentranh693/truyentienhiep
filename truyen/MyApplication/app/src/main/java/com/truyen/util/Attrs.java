package com.truyen.util;

import android.databinding.BindingAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.truyen.R;
import com.truyen.models.Story;

import java.io.File;

/**
 * MyApplication - com.truyen.util
 * Created by Kerofrog on 7/10/17.
 */

public class Attrs {

    @BindingAdapter("url")
    public static void setImage(final ImageView img, Story story) {
        img.setImageResource(0);
        String imgPath = story.getFolder() + FileHandler.getFileName(story.getImage());
        File f = new File(imgPath);
        if (f.exists()) {
            Glide.with(img.getContext().getApplicationContext())
                    .load(f)
                    .into(img);
            return;
        }
        StorageReference ref = FirebaseStorage.getInstance().getReference().child(story.getImage());
        ref.getDownloadUrl().addOnSuccessListener(uri -> Glide.with(img.getContext().getApplicationContext())
                .load(uri.toString())
                .into(img)).addOnFailureListener(Log::e);
    }

    @BindingAdapter("storyState")
    public static void setImg(ImageView img, int state) {
        if (state == Story.State.NORMAL) {
            img.setImageResource(R.drawable.ic_download);
        } else if (state == Story.State.DOWNLOADED) {
            img.setImageResource(R.drawable.ic_delete_button);
        }
    }

    /*
    * @BindingAdapter("url")
fun setImage(img: ImageView, path: String) {

    val ref = FirebaseStorage.getInstance().reference.child(path)
    ref.downloadUrl.addOnSuccessListener {
        Glide.with(img.context)
                .load(it.toString())
                .into(img)
    }.addOnFailureListener {
        log("ex addOnFailureListener $it")
    }
}

@BindingAdapter("html")
fun setText(tv: TextView, text: String) {
    if (Build.VERSION.SDK_INT >= 24) {
        tv.text = Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY)
    } else {
        tv.text = Html.fromHtml(text)
    }
}
    * */

}
